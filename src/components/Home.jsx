import React, { useEffect, useState, useRef, useMemo, useCallback } from 'react';
import '../css/Home.css';
//components
import Header from './Header';
import Product from './Product';
import DashBoard from './DashBoard';
import Footer from './Footer';
//Firebase
import { authFirebase, dbFirebase } from '../lib/firebase';
//toas
import { toast } from 'react-toastify';

const Home = () => {

    let user = authFirebase.currentUser;
    let email = '';


    if (user !== null) {
        user.providerData.forEach((profile) => {
            email = profile.email;
        });
    }
    const [products, setProducts] = useState([]);
    const [currentId, setCurrentId] = useState('');
    const [search, setSearch] = useState('');
    const searchInput = useRef(null);

    const addProduct = async (product) => {
        try {
            if (currentId === '') {
                const idGenerate = Math.floor(Math.random() * 9000).toString();
                await dbFirebase.collection("products").doc(idGenerate).set(product);
                toast('Nuevo Producto añadido', {
                    type: 'success'
                });
            } else {
                await dbFirebase.collection("products").doc(currentId).update(product);
                toast("Producto ha sido actualizado", {
                    type: "info",
                })
                setCurrentId("");
            }
        } catch (error) {
            toast(`Se ha presentado un error ${error}`, {
                type: 'error',
                autoClose: 7000,
            })
        }
    }

    const getProducts = () => {
        dbFirebase.collection("products").onSnapshot((querySnapShot) => {
            const docs = [];
            querySnapShot.forEach((doc) => {
                docs.push({...doc.data(), id:doc.id});
            });
            setProducts(docs);
        });
    };

    useEffect(() => {
        getProducts();
    }, []);

    const handleSearch = useCallback(() => {
        setSearch(searchInput.current.value);
    }, [])

    const filteredUsers = useMemo(() =>
        products.filter((user) => {
            return user.title.toLowerCase().includes(search.toLowerCase());
        }),
        [products, search]
    )

    return (
        <div className="home">
            <Header search={search} searchInput={searchInput} handleSearch={handleSearch} />
            {
                email !== 'test@gmail.com' ?
                    <>
                        <picture className="home__image">
                            <img
                                src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Fuji/2020/May/Hero/Fuji_TallHero_Language_v2_es_US_1x._CB430114961_.jpg"
                                alt="imagen"
                            />
                        </picture>

                        <div className="home__row">
                            { filteredUsers.map(product => (
                                <Product
                                    key={product.id}
                                    id={product.id}
                                    title={product.title}
                                    description={product.description}
                                    price={parseInt(product.price)}
                                    rating={parseInt(product.rating)}
                                    image={product.url}
                                />
                            ))}
                        </div>
                    </>
                    : <DashBoard {...{ filteredUsers, addProduct, currentId }} />
            }

            <Footer />

        </div>
    )

}

export default Home
