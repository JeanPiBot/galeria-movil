import React from 'react';
import { useStateValue } from '../contexts/StateProvider';
import '../css/Checkout.css';
import CheckoutProduct from './CheckoutProduct';
import Subtotal from './Subtotal';

const Checkout = () => {
    const [{ basket }] = useStateValue();


    return (
        <div className="checkout__empty">
            {basket?.length === 0 ? (
                <div>
                    <h2>No tienes nada en tu Shopping Basket</h2>
                    <p>No tienes ningún item en tu Basket. Para comprar uno o más items debes dar click "Agregar a Basket" que se encuentra cerca al item solicitado</p>
                </div>
            ) : (
                    <div className="checkout">
                        <div className="checkout__left">
                            <div>
                                <h2 className="checkout__title">Tu Shopping Basket</h2>
                                {basket?.map((item) => (
                                    <CheckoutProduct
                                        key={item.id}
                                        id={item.id}
                                        title={item.title}
                                        image={item.image}
                                        price={item.price}
                                        rating={item.rating}
                                    />
                                ))}
                            </div>
                        </div>
                        <div className="checkout__right">
                            <Subtotal />
                        </div>
                    </div>
                )}
        </div>
    );
}

export default Checkout;