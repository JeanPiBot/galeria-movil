import React from 'react';
import '../css/Footer.css';

const Footer = () => {
    return (
        <footer className="footer">
            <div className="footer__copyright">
                <span>Copyright © - Galeria Móvil - Todos los Derechos Reservados.</span>
            </div>
        </footer>
    )
}

export default Footer

