import React from 'react';
import '../css/Header.css';
import { Link } from 'react-router-dom';
import SearchIcon from '@material-ui/icons/Search';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import { useStateValue } from '../contexts/StateProvider';
import { authFirebase } from '../lib/firebase';

const Header = ({ search, searchInput, handleSearch }) => {
    const [{ basket, user }] = useStateValue();

    const handleAuthentication = () => {
        if (user) {
            authFirebase.signOut();
        }
    }

    return (
        <header className="header">
            <div className="header__home">
                <Link to="/">
                    <picture className="header__logo">
                        <img  src="https://i.ibb.co/pn8rbpC/logo-gallery.png" alt="logo de galeria movil" border="0" />
                    </picture>
                </Link>
            </div>

            <div className="header__search">
                <label htmlFor="search" >
                    <input id="search" className="header__searchInput" type="text"
                    value={search} ref={searchInput} onChange={handleSearch}
                    name="search" placeholder="Busca productos" />
                    <SearchIcon className="header__searchIcon" />
                </label>
            </div>

            <nav className="header__nav">
                <Link to={!user && "/login"} className="header__link">
                    <div onClick={handleAuthentication} className="header__option">
                        <span className="header__optionLineOne">Hola, {!user ? 'identificate' : user.email}</span>
                        <span className="header__optionLineTwo">{user ? 'Salir' : 'Inicar Sesión'}</span>
                    </div>
                </Link>

                <Link to="/orders">
                    <div className="header__option">
                        <span className="header__optionLineOne">Devoluciones</span>
                        <span className="header__optionLineTwo">& Pedidos</span>
                    </div>
                </Link>

                <Link to="/checkout" className="header__link">
                    <div className="header__option">
                        <ShoppingBasketIcon></ShoppingBasketIcon>
                        <span className="header__optionLineTwo">{basket?.length}</span>
                    </div>
                </Link>

            </nav>
        </header>
    )
}

export default Header
