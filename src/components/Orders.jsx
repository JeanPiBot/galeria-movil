import React, { useState, useEffect } from 'react';
import '../css/Orders.css';
import { dbFirebase } from '../lib/firebase';
import { useStateValue } from '../contexts/StateProvider';
import Order from './Order';

const Orders = () => {
    const [{ user }] = useStateValue()
    const [orders, setOrders] = useState([]);

    useEffect(() => {

        if (user) {
            dbFirebase
                .collection('users')
                .doc(user?.uid)
                .collection('orders')
                .orderBy('created', 'desc')
                .onSnapshot(snapshot => {
                    setOrders(snapshot.docs.map(doc => ({
                        id: doc.id,
                        data: doc.data()
                    })))
                })
        } else {
            setOrders([])
        }

    }, [user])

    return (
        <div className="orders">
            {user ? <h1>Tus pedidos</h1> : <h1>Debes iniciar sesion para ver tus pedidos</h1>}

            <div className="orders__order">
                {orders?.map(order => (
                    <Order order={order} />
                ))}
            </div>
        </div>
    )
}

export default Orders
