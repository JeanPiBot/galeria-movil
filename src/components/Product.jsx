import React from 'react';
import '../css/Product.css';
import { useStateValue } from '../contexts/StateProvider';
import { authFirebase, dbFirebase } from '../lib/firebase';
import { toast } from 'react-toastify';

const Product = ({ id, title, description, image, price, rating }) => {
    const [{ user }, dispatch] = useStateValue();

    let usuario = authFirebase.currentUser;
    let email = '';

    if (usuario !== null) {
        usuario.providerData.forEach((profile) => {
            email = profile.email;
        });
    }

    const formatter = new Intl.NumberFormat("en-CO", {
        style: "currency",
        currency: "COP",
    });

    const addToBasket = () => {
        dispatch({
            type: 'ADD_TO_BASKET',
            item: {
                id: id,
                title: title,
                description: description,
                image: image,
                price: price,
                rating: rating,
            },
        })
    }

    const deleteProduct = async () => {
        if(window.confirm('Estás seguro de querer eliminar el producto')){
            await dbFirebase.collection("products").doc(id).delete();
            toast('Producto eliminado', {
                type: 'error',
                autoClose: 2000,
            })
        }
    };


    return (
        <div className="product">
            <div className="product__info">
                <h1><strong>{title}</strong></h1>
                <p>{description}</p>
                <p className="product__price">
                    <strong>{formatter.format(price)}</strong>
                </p>
                <div className="product__rating">
                    {
                        Array(rating)
                            .fill()
                            .map((_, index) => (
                                <span key={index} role="img" aria-label="star" aria-labelledby="star">⭐</span>
                            ))
                    }
                </div>
            </div>

            <img src={image} alt={title} />
            {
                email !== 'test@gmail.com' ?
                    <button onClick={addToBasket} disabled={!user}>Add to basket</button>
                    :
                    <>
                        <button onClick={deleteProduct}>Eliminar</button>
                    </>
            }
        </div>
    )
}

export default Product;
