import React, { useEffect } from 'react';
//Styles
import './css/App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
//components
import Home from './components/Home';
import Header from './components/Header';
import Login from './components/Login';
import Checkout from './components/Checkout';
import Payment from './components/Payment';
import Orders from './components/Orders';
import Footer from './components/Footer';
//Database
import { authFirebase } from './lib/firebase';
//StateProvider
import { useStateValue } from './contexts/StateProvider';
//stripe
import { loadStripe } from '@stripe/stripe-js'
import { Elements } from '@stripe/react-stripe-js';
//Toast
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const promise = loadStripe('pk_test_51HX77JEei46N5NqoBKHcLiEzcvAyNKlpFQp9jXgflIeTdA6IuqQdpIYACpEoVU9dEqzLy96LkYVnOG6ht2mYoL4I00kZInb2Sw');

const App = () => {

  const [, dispatch] = useStateValue();

  useEffect(() => {

    authFirebase.onAuthStateChanged(authUser => {

      if (authUser) {
        dispatch({
          type: 'SET_USER',
          user: authUser
        })
      } else {
        dispatch({
          type: 'SET_USER',
          user: null
        })
      }

    })
  }, [dispatch])

  return (
    <Router>
      <main className="app">
        <Switch>
          <Route path="/checkout">
            <Header />
            <Checkout />
            <Footer />
          </Route>
          <Route path="/orders">
            <Header />
            <Orders />
            <Footer />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/payment">
            <Header />
            <Elements stripe={promise}>
              <Payment />
            </Elements>
            <Footer />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
        <ToastContainer />
      </main>
    </Router>

  );
}

export default App;
