const functions = require('firebase-functions');

const express = require('express');
const cors = require('cors');
const stripe = require('stripe')('sk_test_51HX77JEei46N5NqosvnMhzxxy74rdGtYaSfkiwj6hUr1jbIllR9U7wpFIq9LRkCrr6ITlK11dO7T9A4CjPDU8eSk00bSLgtcfK')

//API


//App config
const app = express();

// Middlewares
app.use(cors({ origin: true }));
app.use(express.json());


//API routes
app.get('/', (request, response) => response.status(200).send('Hello world'))

app.post('/payments/create', async (request, response) => {
    const total = request.query.total;

    console.log(`Payment Request recieved Boom!!! for this amount >>> ${total}`)

    const paymentIntent = await stripe.paymentIntents.create({
        amount: total,
        currency: 'USD',
    });

    //Ok -Created
    response.status(201).send({
        clientSecret: paymentIntent.client_secret,
    })
})

//listen comand
exports.api = functions.https.onRequest(app);


